#!/bin/bash

# Get the path of this script
SCRIPTPATH=$(realpath  $(dirname "$0"))
DOCPATH=${SCRIPTPATH}/../doc

# Run docker (this script may need to be launched with sudo if the user is not in the docker group)
docker run -it --user $(id -u):$(id -g) --rm -v ${DOCPATH}:/docs sphinxdoc/sphinx make html
