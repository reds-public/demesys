Other
=====

Setting up documentation with Sphinx
------------------------------------

* `ReadTheDocs - Sphinx Getting Started Guide <https://docs.readthedocs.io/en/stable/intro/getting-started-with-sphinx.html>`_
* `Sphinx - Getting Started Guide <https://www.sphinx-doc.org/en/master/usage/quickstart.html>`_

reStructuredText (RST) guides
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* `Sphinx - reStructuredText Primer <http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_
* `Quick reStructuredText (public domain) <https://docutils.sourceforge.io/docs/user/rst/quickref.html>`_

reStructuredText (RST) conventions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`OpenStack <https://www.openstack.org/>`_ has a nice list of conventions for their reStructuredText (RST) with Sphinx extensions. Available `here <https://docs.openstack.org/doc-contrib-guide/rst-conv.html>`_.
