PC - Personal Computer
======================

Topics include :

* Simulating the embedded system
* Software hierarchy and models (e.g., MVC, handling the GUI and core code separately)
* Unit tests
* Integration tests
