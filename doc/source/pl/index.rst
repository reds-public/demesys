PL - Programmable Logic (FPGAs)
===============================

Topics include :

* Setting up projects Intel/Xilinx from command line
* Command line build of projects (synth / p&r)
* Version control (git) of projects (Intel/Xilinx)
* Synthesis
* Simulation
* Formal Verification
* Verification methodologies (UVM, UVVM, OSVVM, TLMVM)
* Automated build pipelines
* Automated simulation and verification pipelines
  
.. toctree::
   :maxdepth: 2
   :caption: Current contents:

   setup
   cmd
   vc
   formal
