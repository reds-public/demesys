Setting up projects
===================

Setting up projects from the command line has the benefit that scripts are well suited for version control, this also relieves the need to add all the project files to the version control.

Using scripts does not mean the GUI should not be used, it means that changes done with the GUI should be added into scripts or configuration files that can be easily version controlled.

Intel (Altera)
--------------

Intel provides the following guide in their documentation :

* `Intel Quartus Prime Standard Edition User Guide: Scripting <https://www.intel.com/content/www/us/en/programmable/documentation/jeb1529967983176.html>`_

Xilinx
------

Xilinx provides the following guide :

* `UG894 - Vivado Design Suite User Guide - Using Tcl Scripting <https://www.xilinx.com/support/documentation-navigation/see-all-versions.html?xlnxproducttypes=Design%20Tools&xlnxdocumentid=UG894>`_

Xilinx also provides an extended reference guide (roughly 2000 pages) for Tcl :

* `UG835 - Vivado Design Suite Tcl Command Reference Guide <https://www.xilinx.com/support/documentation-navigation/see-all-versions.html?xlnxproducttypes=Design%20Tools&xlnxdocumentid=UG835>`_

.. UG835 - Vivado Design Suite Tcl Command Reference Guide (~2000 pages)
   UG894 - Vivado Design Suite User Guide - Using Tcl Scripting (114 pages)

   Explain how to generate tcl file from block diagrams and how to handle IPs,
   Amazon has great examples for this on their AWS FPGA github page :
   https://github.com/aws/aws-fpga

   Intel : https://www.intel.com/content/www/us/en/programmable/documentation/jeb1529967983176.html
   Intel Quartus Prime Standard Edition User Guide: Scripting
