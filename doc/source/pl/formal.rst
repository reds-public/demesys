Formal Verification
===================

Questa Formal
-------------

`The Questa® Formal Verification tool <https://www.mentor.com/products/fv/questa-formal/>`_ complements simulation-based RTL design verification by analyzing all possible behaviors of the design to detect any reachable error states. This exhaustive analysis ensures that critical control blocks work correctly in all cases and locates design errors that may be missed in simulation.

Questa Formal Examples
^^^^^^^^^^^^^^^^^^^^^^

Examples of formally verifying assertions with Questa Formal are available (for REDS only) in the following repository under verification/exemples/formel :

* https://gitlab.com/reds-general/hdl

SymbiYosys
----------

`SymbiYosys (sby) <https://symbiyosys.readthedocs.io/en/latest/>`_ is a an open-source front-end driver program for Yosys-based formal hardware verification flows. SymbiYosys is released under `ISC License <https://en.wikipedia.org/wiki/ISC_license>`_
