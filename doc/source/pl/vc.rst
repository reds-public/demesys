Version Control of FPGA projects
================================

Intel (Altera)
--------------

Xilinx
------

Version control of Xilinx (Vivado) projects is documented in the following user guide :

* `UG1198 - Vivado Design Suite Tutorial: Revision Control <https://www.xilinx.com/support/documentation-navigation/see-all-versions.html?xlnxproducttypes=Design%20Tools&xlnxdocumentid=UG1198>`_
