.. Demesys documentation master file, created by
   sphinx-quickstart on Wed Mar 25 10:29:23 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Demesys's documentation!
===================================

This will be the documentation for the Demesys project.

A brainstorming and idea Google document is available at : google-doc_.

.. _google-doc: https://docs.google.com/document/d/1yY9_kYh-ndR8EwLKu73JVlmvc7zcCaLDdzzQ_oPX_TQ/edit?usp=sharing

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   pl/index
   pl-ps/index
   ps/index
   ps-pc/index
   pc/index
   general/index
   other/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
