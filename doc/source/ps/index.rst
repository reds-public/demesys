PS - Processing Systems (Embedded Processors, Microcontrollers)
===============================================================

Topics include :

* Simulating the PL side for the embedded code (in software)
* Emulating PL behavior with dummy (or simplified) PL bitstream image
* Running the embedded code in an emulator (qemu)
* Emulating the connection to a PC (netowrk, uart, etc.)
* Building the uBoot image
* Building the Linux kernel image (buildroot, yocto)

