# Demesys

Development methodologies for embedded systems

This project aims to give guidelines, recipes, advice, and general methodologies for the development, version control, testing, documentation, verification, continuous integration, and validation of embedded systems.

## Documentation

This project will be documented with [Sphinx](https://www.sphinx-doc.org/). [Sphinx is not only for python](https://www.ericholscher.com/blog/2014/feb/11/sphinx-isnt-just-for-python/).

This is only the `README.md` page and will only provide basic information, refer to the documentation in `docs` for further information.

### Building the documentation

The documentation can be built using the script `scripts/makehtmldoc.sh`. This bash script relies on [Docker](https://www.docker.com/) and Docker must be installed for it to work. The documentation can also be built using the traditional method and a Makefile is provided in the `docs` directory.

The documentatation is built into the `docs/build` directory and can be accessed through the index.html file.

### Removing the documentation output products

In order to remove the generated documentation files simply remove the contents in `docs/build/` for example with :

```bash
$ rm -rf docs/build/*
```

It is also possible to use the script `scripts/cleanhtmldoc.sh` which does the same thing.
